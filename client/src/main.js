import Vue from 'vue'
import App from './App.vue'
import store from './store';
import router from './router';
import axios from 'axios';
import {interceptorsUtil, preventAuthUser} from './utils';
import vuetify from '@/plugins/vuetify';



Vue.use({store});
Vue.config.productionTip = false;
Vue.prototype.$axios = axios;

interceptorsUtil();

router.beforeEach((to, from, next) => {
  const userInfo = localStorage.getItem('user');
  preventAuthUser(to.path, userInfo, next)
});

new Vue({
  store,
  router,
  vuetify,
  render: h => h(App),
  created() {
    const userInfo = localStorage.getItem('user');

    if (userInfo) {
      const userData = JSON.parse(userInfo);
      this.$store.commit('setUserData', userData)
    }
  },
}).$mount('#app');
