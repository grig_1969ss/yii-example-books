import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
    {
        path: '/login',
        name: 'Login',
        meta: {layout: 'auth'},
        component: () => import('../views/auth/Login')
    },
    {
        path: '/register',
        name: 'Register',
        meta: {layout: 'auth'},
        component: () => import('../views/auth/Register')
    },
    {
        path: '/',
        name: 'Genres',
        meta: {layout: 'main'},
        component: () => import('../views/genres'),
    },

    {
        path: '/create',
        name: 'create-genres',
        meta: {layout: 'main'},
        component: () => import('../views/genres/create'),
    },

    {
        path: '/edit/:id',
        name: 'edit-genres',
        meta: {layout: 'main'},
        component: () => import('../views/genres/edit'),
    },

    {
        path: '/books',
        name: 'Books',
        meta: {layout: 'main'},
        component: () => import('../views/books'),
    },

    {
        path: '/books/create',
        name: 'create-books',
        meta: {layout: 'main'},
        component: () => import('../views/books/create'),
    },

    {
        path: '/books/edit/:id',
        name: 'edit-books',
        meta: {layout: 'main'},
        component: () => import('../views/books/edit'),
    },

    {
        path: '/books/:id',
        name: 'books-item',
        meta: {layout: 'main'},
        component: () => import('../views/books/id'),
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

export default router
