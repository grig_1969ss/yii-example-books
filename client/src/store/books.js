import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const version = process.env.VUE_APP_API_VERSION;

export const Books = {
    state: () => ({
        bookList: null,
        booksItem: null,
        createBooksMessage: null,
        deleteBookMessage: null,
        updateBookMessage: null,
        updateBookErrorMessages: null,
        createBookErrorMessages: null,

    }),

    mutations: {
        setBooksList(state, data) {
            state.bookList = data;
        },
        setBooksItem(state, data) {
            state.booksItem = data;
        },

        setCreateBooks(state, data) {
            state.createBooksMessage = data;
            state.createBookErrorMessages = null;
        },
        setBooksDeleteData(state, data) {
            state.deleteBookMessage = data;
        },
        setBooksUpdateData(state, data) {
            state.updateBookMessage = data;
            state.updateBookErrorMessages = null;
        },
        setBooksUpdateError(state, data) {
            state.updateBookErrorMessages = data;
        },

        setBooksCreateError(state, data) {
            state.createBookErrorMessages = data;
        },
    },

    actions: {
        async getBooksItem({commit}, id) {
            return await axios
                .get(`/${version}/books/view?id=${id}`)
                .then(response => {
                    commit('setBooksItem', response.data)
                })
                .catch(err => console.log(err))
        },

        async deleteBook({commit}, id) {
            return await axios
                .delete(`/${version}/books/delete?id=${id}`)
                .then(response => {
                    commit('setBooksDeleteData', response.data)
                })
                .catch(err => console.log(err))
        },

        async updateBook({commit}, {id, title, isbn, genre_ids}) {
            return await axios
                .put(`/${version}/books/update?id=${id}`, {title, isbn, genre_ids})
                .then(response => {
                    commit('setBooksUpdateData', response.data)
                })
                .catch(err => {
                    commit('setBooksUpdateError', err.response.data)
                })
        },

        async createBooks({commit}, data) {
            return await axios
                .post(`/${version}/books/create`, data)
                .then(response => {
                    commit('setCreateBooks', response.data)
                })
                .catch(err => {
                    commit('setBooksCreateError', err.response.data)
                })
        }
    },

    getters: {
        bookList: state => state.bookList,
        booksItem: state => state.booksItem,
        updateBooksErr: state => state.updateBookErrorMessages,
        createBooksErr: state => state.createBookErrorMessages,
    }

}
