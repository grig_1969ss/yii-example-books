import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import {Genres} from './genres'
import {Books} from './books'

Vue.use(Vuex);

axios.defaults.baseURL = process.env.VUE_APP_API_URL;
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.common['crossDomain'] = true;

const modules = {
    Genres,
    Books,
};

const state = {
    user: null,
    authKey: null,
    errorMessageRegister: null,
    errorMessageLogin: null,
};

const mutations = {
    setUserData(state, data) {
        state.user = data;
        localStorage.setItem('user', JSON.stringify(data));
        axios.defaults.headers.common.Authorization = `Bearer ${data}`
    },
    setRemoveUserData(state, data) {
        state.user = null;
        localStorage.remove('user');
        axios.defaults.headers.common.Authorization = `Bearer ${data}`
    },
    setAuthKey(state, data) {
        state.authKey = data;
    },
    setErrorMsgRegister(state, data){
        state.errorMessageRegister = data;
    },
    setErrorLogin(state, data){
        state.errorMessageLogin = data;
    },
};

const actions = {
    async login({commit}, data) {
        return await axios
            .post('/login', data)
            .then( response => {
                commit('setUserData', response.data.data.token);
            })
            .catch(err => {
                commit('setErrorLogin', err.response.data.message)
            })

    },

    logout({commit}) {
        return commit('setRemoveUserData')
    },

    async register({commit}, data) {
        return await axios
            .post('/signup', data)
            .then( response => {
                commit('setAuthKey', response.data.data.auth_key)
            }).catch(err => {
                commit('setErrorMsgRegister', err.response.data.message)
            })

    },
};

const getters = {
    user: state => state.user,
    errorMessageRegister: state => state.errorMessageRegister,
    authKey: state => state.authKey,
    msgLogin: state => state.errorMessageLogin,
};

export default new Vuex.Store({
    modules,
    state,
    mutations,
    actions,
    getters
})
