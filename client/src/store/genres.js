import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);
const version = process.env.VUE_APP_API_VERSION;

 export const Genres = {
    state: () => ({
        genresList: null,
        genresItem: null,
        deleteGenresMessage: null,
        createGenresMessage: null,
        updateGenresMessage: null,
        updateGenresErrorMessage: null,
        createGenresErrorMessage: null,
    }),

    mutations: {

        setGenresList(state, data) {
            state.genresList = data;
        },
        setGenresItem(state, data) {
            state.genresItem = data;
        },
        setGenresDeleteData(state, data) {
            state.deleteGenresMessage = data;
        },
        setCreateGenres(state, data) {
            state.createGenresMessage = data;
            state.createGenresErrorMessage = null;
        },
        setGenresUpdateData(state, data) {
            state.updateGenresMessage = data
            state.updateGenresErrorMessage = null;
        },
        setGenresUpdateError(state, data) {
            state.updateGenresErrorMessage = data;
        },
        setGenresCreateError(state, data) {
            state.createGenresErrorMessage = data;
        },
    },

    actions: {
        async getGenres({commit}) {
            return await axios
                .get(`/${version}/genres`)
                .then(response => {
                    commit('setGenresList', response.data)
                })
                .catch(err => console.log(err))
        },

        async getGenresItem({commit}, id) {
            return await axios
                .get(`/${version}/genres/view?id=${id}`)
                .then(response => {
                    commit('setGenresItem', response.data)
                })
                .catch(err => console.log(err))
        },

        async getBooks({commit}) {
            return await axios
                .get(`/${version}/books`)
                .then(response => {
                    commit('setBooksList', response.data)
                })
                .catch(err => console.log(err))
        },

        async deleteGenres({commit}, id) {
            return await axios
                .delete(`/${version}/genres/delete?id=${id}`)
                .then(response => {
                    commit('setGenresDeleteData', response.data)
                })
                .catch(err => console.log(err))

        },

        async updateGenres({commit}, {id, name}) {
            return await axios
                .put(`/${version}/genres/update?id=${id}`, {name})
                .then(response => {
                    commit('setGenresUpdateData', response.data)
                })
                .catch(err => {
                    commit('setGenresUpdateError', err.response.data)
                })

        },

        async createGenres({commit}, data) {
            return await axios
                .post(`/${version}/genres/create`, data)
                .then(response => {
                    commit('setCreateGenres', response.data)
                })
                .catch(err => {
                    commit('setGenresCreateError', err.response.data)
                })
        },

    },
    getters: {
        genresList: state => state.genresList,
        genresItem: state => state.genresItem,
        updateGenresErr: state => state.updateGenresErrorMessage,
        createGenresErr: state => state.createGenresErrorMessage,
    },
}
