import axios from "axios";
import router from "../router";

export const interceptorsUtil = function () {
    axios.interceptors.response.use(
        function (response) {
            // Call was successful, don't do anything special.
            return response;
        },
        function (error) {
            switch (error.response.status) {
                case 419: // Session expired
                case 503: // Down for maintenance
                    // Bounce the user to the login screen with a redirect back
                    router.push({path: '/login'});
                    break;
                case 500:
                    console.log('Oops, something went wrong!');
                    break;
                default:
                    // Allow individual requests to handle other errors
                    return Promise.reject(error);
            }
        }
    );
};


export const preventAuthUser = function (to, user, next) {
    if (user && (to === '/login' || to === '/register')) {
        next('/');
    }

    if (user) {
        next()
    } else {
        if (to !== '/login' && to !== '/register') {
            next('/login');
        } else {
            next();
        }
    }
};
