<?php


namespace app\modules\v1\resource;


use yii\db\ActiveRecord;

/**
 * Class BookGenre
 * @package app\modules\v1\resource
 */
class BookGenre extends ActiveRecord
{
    public static function tableName()
    {
        return 'book_genre';
    }
}