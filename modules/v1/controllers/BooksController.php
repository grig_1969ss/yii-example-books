<?php

namespace app\modules\v1\controllers;


use app\models\BookGenre;
use app\modules\v1\resource\Book;
use yii\web\NotFoundHttpException;

class BooksController extends ApiController
{
    public $modelClass = Book::class;

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create']);
        unset($actions['update']);
        unset($actions['view']);

        return $actions;
    }

    public function actionCreate()
    {
        $book = new Book();

        $book->load(\Yii::$app->request->post(), '');

        if($book->save()) {
            $row = [];
            foreach ($book->genre_ids as $genre_id) {
                $row[] = [$book->id, $genre_id];
            }

            \Yii::$app->db->createCommand()
                ->batchInsert('book_genre',
                ['book_id', 'genre_id'],
                    $row)->execute();

        }
        return $book;
    }

    public function actionUpdate($id)
    {
        $book = Book::findOne($id);


        $book->load(\Yii::$app->request->post(), '');


        if($book->save()) {
            $row = [];
            foreach ($book->genre_ids as $genre_id) {
                $row[] = [$book->id, $genre_id];
            }

            BookGenre::deleteAll(['book_id' => $book->id]);

            \Yii::$app->db->createCommand()
                ->batchInsert('book_genre',
                ['book_id', 'genre_id'],
                    $row)->execute();

        }
        return $book;
    }

    public function actionView($id)
    {
        if(!$book = Book::findOne($id)) {
            throw new NotFoundHttpException();
        }

        return [
            'id' => $book->id,
            'isbn' => $book->isbn,
            'title' => $book->title,
            'author' => $book->user->username,
            'created_at' =>  $book->created_at,
            'genres' => $book->genres
        ];
    }

}