<?php

namespace app\modules\v1\controllers;

use app\modules\v1\resource\Genre;

class GenresController extends ApiController
{
    public $modelClass = Genre::class;

}