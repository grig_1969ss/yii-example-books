<?php

namespace app\modules\v1\controllers;

use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;

abstract class ApiController extends ActiveController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['bearerAuth'] =
            [
                'class' =>  HttpBearerAuth::class,
            ];
        return $behaviors;
    }
}