<?php

namespace app\modules\v1\controllers;

use app\models\Status;
use app\modules\v1\resource\User;
use yii\rest\Controller;
use yii\filters\auth\HttpBearerAuth;

class UsersController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['bearerAuth'] = [
                'class' => HttpBearerAuth::class,
            ];
        return $behaviors;
    }

    public function actionIndex()
    {

        $users = User::find()->all();
        return [
            'status' => Status::STATUS_OK,
            'message' => 'Success',
            'data' => $users
        ];
    }

}