<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%book_genre}}`.
 */
class m210212_122405_create_book_genre_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%book_genre}}', [
            'book_id' => $this->integer()->unsigned(),
            'genre_id' => $this->integer()->unsigned(),
        ]);

        $this->addForeignKey(
            'fk-book_id',
            'book_genre',
            'book_id',
            'books',
            'id',
            'CASCADE'
        );


        $this->addForeignKey(
            'fk-genre_id',
            'book_genre',
            'genre_id',
            'genres',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%book_genre}}');
    }
}
