<?php

namespace app\models;


use yii\db\ActiveRecord;

class Genre extends ActiveRecord
{

    public static function tableName()
    {
        return 'genres';
    }

    /**
     * Returns the validation rules for attributes.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 100],
            ['name', 'unique'],
        ];
    }

    public function getBooks()
    {
        return $this->hasMany(Book::class, ['book_id' => 'id'])
            ->viaTable('book_genre', ['id' => 'genre_id']);
    }

}